class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	validates_presence_of :title
	validates_presence_of :body

	validates_presence_of :title 
	validates_length_of :title, :in => 5..30, :message => "Longitud invalida"
	validates_presence_of :body
	validates :body, length: { maximum: 50, message: "El texto del post es muy largo"}
	validates :body, length: { minimum: 10, message: "El texto del post es muy corto"}
end
